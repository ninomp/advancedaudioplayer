#ifndef PLAYERTHREAD_H
#define PLAYERTHREAD_H

#include <QThread>

#include "equalizerbands.h"

class AudioData;
class AudioFilter;
class AudioOutput;

class PlayerThread : public QThread
{
	Q_OBJECT

	enum State { Idle, Ready, Playing, StoppingCue };

public:
	explicit PlayerThread(QObject *parent = 0);
	~PlayerThread();

	void setInput(const AudioData*);
	void clearInput();

	void setPosition(int);

	void setGain(float);
	void setTempo(float);
	void setPitch(float);

	void setEqualizerBands(EqualizerBands);

public slots:
	void playpause();
	void cueStart();
	void cueStop();

	void seekingBegin();
	void seekingStep(int);
	void seekingEnd();

	void setEqualizerEnabled(bool);

signals:
	void started();
	void stopped();

	void positionChanged(int samples);
	void durationChanged(int samples);

	void bufferSent(short * data, int channels, int samples);

	void leftLevelsChanged(unsigned short rms, unsigned short peak);
	void rightLevelsChanged(unsigned short rms, unsigned short peak);

private:
	static const int BufferLength;  // miliseconds

	void run();

	bool createBuffer(int channels, int buffer_size /* samples in each channel */);
	void deleteBuffer();

	State m_state;
	State m_previousState;

	AudioOutput * m_output;

	AudioFilter  *  m_filter;
	EqualizerBands  m_filterBands;
	bool            m_filterEnabled;

	const AudioData * m_data;

	int m_position;
	int m_cue;
	int m_step;
	int m_gain;
	int m_resamplerate;

	short * m_bufferData;
	int     m_bufferChannels;
	int     m_bufferLength;  // samples per channel
};

#endif // PLAYERTHREAD_H
