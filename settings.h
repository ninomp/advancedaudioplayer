#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

#define SETTING_INT(name, default_value) \
	int  get##name()           {  return m_pSettings->value(#name, default_value).toInt();  } \
	void set##name(int value)  {         m_pSettings->setValue(#name, value);               }

class Settings
{
public:
	Settings(QString filename);
	~Settings();

	SETTING_INT(GainRange,   5)
	SETTING_INT(SpeedRange, 25)
	SETTING_INT(TempoRange, 25)
	SETTING_INT(PitchRange, 25)
	SETTING_INT(EqRange,     5)

private:
	QSettings * m_pSettings;
};

#endif // SETTINGS_H
