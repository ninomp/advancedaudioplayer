#include "playerthread.h"
#include "audio/audiodata.h"
#include "audio/audiofilter.h"
#include "audio/audiooutput.h"

const int PlayerThread::BufferLength = 74;  // miliseconds

PlayerThread::PlayerThread(QObject *parent) :
    QThread(parent)
{
	m_state          = Idle;
	m_output         = new AudioOutput;
	m_filter         = new AudioFilter;
	m_filterEnabled  = true;
	m_data           = 0;
	m_gain           = 1024;
	m_resamplerate   = 1024;
	m_bufferData     = 0;
	m_bufferChannels = 0;
	m_bufferLength   = 0;
}

PlayerThread::~PlayerThread()
{
	delete m_filter;
	delete m_output;
	deleteBuffer();
}

void PlayerThread::setInput(const AudioData * pAudioData)
{
	int buffer_size = (pAudioData->samplerate() * BufferLength) / 1000;
	m_data = pAudioData;
	m_position = 0;
	m_cue = 0;
	m_step = buffer_size;
	createBuffer(pAudioData->channels(), buffer_size);
	m_output->open(pAudioData->channels(), pAudioData->samplerate());
	m_filter->prepare(buffer_size);
	m_filter->set(m_filterBands, pAudioData->samplerate());
	m_state = Ready;
	emit durationChanged(pAudioData->samples());
}

void PlayerThread::clearInput()
{
	m_state = Idle;
	m_filter->cleanup();
	m_output->close();
	deleteBuffer();
	m_data = 0;
	emit durationChanged(0);
}

void PlayerThread::setPosition(int position)
{
	if (m_state >= Ready)
	{
		if      (position < 0)                  position = 0;
		else if (position > m_data->samples())  position = m_data->samples();
		m_position = position;
		emit positionChanged(position);
	}
}

void PlayerThread::setGain(float gain)
{
	m_gain = 1024.0f * gain;
}

void PlayerThread::setTempo(float tempo)
{
	m_step = m_bufferLength * (1.0f + tempo);
}

void PlayerThread::setPitch(float pitch)
{
	m_resamplerate = 1024.0f * (1.0f + pitch);
}

void PlayerThread::setEqualizerBands(EqualizerBands bands)
{
	if (m_data)  m_filter->set(bands, m_data->samplerate());
	m_filterBands = bands;
}

void PlayerThread::setEqualizerEnabled(bool enabled)
{
	m_filterEnabled = enabled;
}

void PlayerThread::playpause()
{
	if (m_state >= Ready)
	{
		if (m_state != Playing)
		{
			m_state = Playing;
			start();
		}
		else
		{
			m_state = Ready;
		}
	}
}

void PlayerThread::cueStart()
{
	if (m_state >= Ready)
	{
		m_cue = m_position;
		m_previousState = m_state;
		m_state = Playing;
		start();
	}
}

void PlayerThread::cueStop()
{
	if (m_state >= Ready)
	{
		m_state = StoppingCue;
	}
}

void PlayerThread::seekingBegin()
{
	if (m_state >= Ready)
	{
		m_previousState = m_state;
		m_state = Ready;
	}
}

void PlayerThread::seekingStep(int position)
{
	setPosition(position);
}

void PlayerThread::seekingEnd()
{
	if (m_state >= Ready)
	{
		m_state = m_previousState;
		if (m_state == Playing)  start();
	}
}

void PlayerThread::run()
{
	emit started();

	while (m_state == Playing)
	{
		if (m_position < m_data->samples())
		{
			short * source_data = m_data->data() + m_data->channels() * m_position;
			int     source_len  = m_data->samples() - m_position;

			resampleWithGain(m_bufferData, m_bufferLength, source_data, source_len, m_bufferChannels, m_resamplerate, m_gain);

			if (m_filterEnabled)
			{
				if (m_bufferChannels == 1)
					m_filter->filterMono(m_bufferData);
				else if (m_bufferChannels == 2)
					m_filter->filterStereo((StereoSampleBlock*) m_bufferData);
			}

			m_output->write(m_bufferData, m_bufferLength);
			emit bufferSent(m_bufferData, m_bufferChannels, m_bufferLength);

			AudioLevel level;
			measureAudioLevels(&level, m_bufferData, m_bufferChannels, m_bufferLength);
			emit leftLevelsChanged (level.left_rms,  level.left_peak);
			emit rightLevelsChanged(level.right_rms, level.right_peak);

			emit positionChanged(m_position);

			m_position += m_step;
		}
		else
		{
			m_state = Ready;
		}
	}

	emit stopped();

	emit leftLevelsChanged (0, 0);
	emit rightLevelsChanged(0, 0);

	if (m_state == StoppingCue)
	{
		m_state = Ready;
		m_position = m_cue;
		emit positionChanged(m_cue);
	}
}

bool PlayerThread::createBuffer(int channels, int buffer_size)
{
	deleteBuffer();

	m_bufferData = new short[channels * buffer_size];
	if (m_bufferData)
	{
		m_bufferChannels = channels;
		m_bufferLength   = buffer_size;

		return true;
	}
	else  return false;
}

void PlayerThread::deleteBuffer()
{
	if (m_bufferData)  delete m_bufferData;
	m_bufferData     = 0;
	m_bufferChannels = 0;
	m_bufferLength   = 0;
}
