#-------------------------------------------------
#
# Project created by QtCreator 2013-08-28T22:24:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AdvancedAudioPlayer
TEMPLATE = app

unix:LIBS += -lao -lsndfile -lmpg123 -lfftw3

win32 {
    INCLUDEPATH += \
        ..\lib\libao-master\src\libao \
        ..\lib\libsndfile\include \
        ..\lib\mpg123-1.15.1-x86 \
        ..\lib\fftw-3.3.3-dll32

    Debug:LIBS   += ..\..\lib\libao\build\temp\lib\libao-mt-debug.lib
    Release:LIBS += ..\..\lib\libao\build\temp\lib\libao-mt.lib

    LIBS += \
        winmm.lib \
        ..\..\lib\libsndfile\lib\libsndfile-1.lib \
        ..\..\lib\mpg123-1.15.1-x86\libmpg123-0.lib \
        ..\..\lib\fftw-3.3.3-dll32\libfftw3-3.lib
}

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    preferencesdialog.cpp \
    settings.cpp \
    playerthread.cpp \
    audio/audiodata.cpp \
    audio/audiofileloadersndfile.cpp \
    audio/audiofileloadermpg123.cpp \
    audio/audiofilter.cpp \
    audio/audiooutput.cpp \
    widgets/rightclickableslider.cpp \
    widgets/meter.cpp \
    widgets/oscilloscope.cpp \
    widgets/waveform.cpp \
    widgets/spectrum.cpp \
    widgets/equalizer.cpp

HEADERS  += \
    mainwindow.h \
    preferencesdialog.h \
    settings.h \
    playerthread.h \
    audio/audiodata.h \
    audio/audiofileloader.h \
    audio/audiofileloadersndfile.h \
    audio/audiofileloadermpg123.h \
    audio/audiofilter.h \
    audio/audiooutput.h \
    widgets/rightclickableslider.h \
    widgets/meter.h \
    widgets/oscilloscope.h \
    widgets/waveform.h \
    widgets/spectrum.h \
    widgets/equalizer.h \
    equalizerbands.h
