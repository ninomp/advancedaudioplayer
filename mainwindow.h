#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

// Forward declarations of Qt classes
class QLabel;
class QPushButton;
class QSlider;

// Forward declarations of internal classes
class Settings;
class PreferencesDialog;
class AudioFileLoaderSNDFILE;
class AudioFileLoaderMPG123;
class AudioData;
class PlayerThread;
class RightClickableSlider;
class Meter;
class Oscilloscope;
class Waveform;
class Spectrum;
class Equalizer;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

	// Preferences interface
	int  getGainRange();
	void setGainRange(int);
	int  getSpeedRange();
	void setSpeedRange(int);
	int  getTempoRange();
	void setTempoRange(int);
	int  getPitchRange();
	void setPitchRange(int);
	int  getEqRange   ();
	void setEqRange   (int);

private slots:
	void load();
	void unload();
	void preferences();
	void about();

	void slotGainChanged(int);
	void slotSpeedChanged(int);
	void slotTempoChanged(int);
	void slotPitchChanged(int);

	void slotSpeedSliderMoved(int);
	void slotTempoSliderMoved(int);
	void slotPitchSliderMoved(int);

	void slotGainSliderRightClicked();
	void slotSpeedSliderRightClicked();
	void slotTempoSliderRightClicked();
	void slotPitchSliderRightClicked();

	void slotEqualizerChanged();

	void slotPositionChanged(int);
	void slotDurationChanged(int);

private:
	void createActions();
	void createMenus();
	void createWidgets();
	void createLayouts();

	void updateLabelPosition(int    value_samples  /* smp */);
	void updateLabelGain    (double value_decibels /* dB  */);
	void updateLabelSpeed   (float  value_percent  /*  %  */);
	void updateLabelTempo   (float  value_percent  /*  %  */);
	void updateLabelPitch   (float  value_percent  /*  %  */);

	// Constants
	static const QString WindowTitle;
	static const QString AboutTitle;
	static const QString AboutText;
	static const QString FileDialogFilter;

	static const int SliderScaleFactorGain;
	static const int SliderScaleFactorSpeed;
	static const int SliderScaleFactorTempo;
	static const int SliderScaleFactorPitch;

	// Settings & Preferences
	Settings             * m_pSettings;
	PreferencesDialog    * m_pPreferencesDialog;

	// Actions
	QAction              * m_pActionOpen;
	QAction              * m_pActionClose;
	QAction              * m_pActionExit;
	QAction              * m_pActionViewScope;
	QAction              * m_pActionViewWaveform;
	QAction              * m_pActionViewSpectrum;
	QAction              * m_pActionViewEqualizer;
	QAction              * m_pActionEqualizerEnable;
	QAction              * m_pActionPreferences;
	QAction              * m_pActionAbout;
	QAction              * m_pActionAboutQt;
	//QAction              * m_pAction;

	// Widgets
	QPushButton          * m_pButtonOpen;
	QPushButton          * m_pButtonCue;
	QPushButton          * m_pButtonPlayPause;
	QPushButton          * m_pButtonExit;
	QLabel               * m_pLabelPosition;
	QLabel               * m_pLabelGain;
	QLabel               * m_pLabelSpeed;
	QLabel               * m_pLabelTempo;
	QLabel               * m_pLabelPitch;
	QSlider              * m_pSliderPosition;
	RightClickableSlider * m_pSliderGain;
	RightClickableSlider * m_pSliderSpeed;
	RightClickableSlider * m_pSliderTempo;
	RightClickableSlider * m_pSliderPitch;
	Meter                * m_pMeterL;
	Meter                * m_pMeterR;
	Oscilloscope         * m_pOscilloscope;
	Waveform             * m_pWaveform;
	Spectrum             * m_pSpectrum;
	Equalizer            * m_pEqualizer;
	QDockWidget          * m_pEqualizerDock;

	// Audio
	AudioFileLoaderSNDFILE * m_pLoaderSNDFILE;
	AudioFileLoaderMPG123  * m_pLoaderMPG123;
	AudioData              * m_pAudioData;
	PlayerThread           * m_pPlayerThread;

	QString m_strDirectory;
	QString m_strDuration;

	int m_nPitchDifference;

	// Static helpers
	static void    setSliderRange(QSlider*, int range, int scale);
	static QString formatTime(int samples, int samplerate);
};

#endif // MAINWINDOW_H
