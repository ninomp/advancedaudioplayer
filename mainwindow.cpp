#include <QtCore/qmath.h>
#include <QApplication>
#include <QAction>
#include <QMenuBar>
#include <QMessageBox>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QLayout>
#include <QDockWidget>
#include <QFileDialog>

#include "mainwindow.h"
#include "settings.h"
#include "preferencesdialog.h"
#include "playerthread.h"
#include "audio/audiofileloadersndfile.h"
#include "audio/audiofileloadermpg123.h"
#include "widgets/rightclickableslider.h"
#include "widgets/meter.h"
#include "widgets/oscilloscope.h"
#include "widgets/waveform.h"
#include "widgets/spectrum.h"
#include "widgets/equalizer.h"

const QString MainWindow::WindowTitle      = "Advanced Audio Player";
const QString MainWindow::AboutTitle       = "About";
const QString MainWindow::AboutText        = "Advanced Audio Player\nCross-platform audio player with speed/tempo/pitch control, VU/Peak meter, oscilloscope, spectrum analyzer and equalizer\nUsing Qt, libmpg123, libsndfile, libao and fftw.\nVersion 1.0\nAuthor: Nino Miskic-Pletenac <nino.mip@gmail.com>\nCopyright (C) 2013.";
const QString MainWindow::FileDialogFilter = "All supported audio (*.wav *.flac *.ogg *.mp1 *.mp2 *.mp3);;All files (*.*);;Wave (*.wav);;FLAC (*.flac);;Ogg/Vorbis (*.ogg);;MPEG Audio (*.mp1 *.mp2 *.mp3)";

const int MainWindow::SliderScaleFactorGain  = 100;
const int MainWindow::SliderScaleFactorSpeed = 10;
const int MainWindow::SliderScaleFactorTempo = 10;
const int MainWindow::SliderScaleFactorPitch = 10;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
	m_strDirectory = QDir::homePath();

	m_nPitchDifference = 0;
	m_pAudioData       = 0;

	m_pLoaderSNDFILE = new AudioFileLoaderSNDFILE;
	m_pLoaderMPG123  = new AudioFileLoaderMPG123;

	m_pSettings          = new Settings("player.ini");
	m_pPreferencesDialog = new PreferencesDialog(this);

	m_pPlayerThread  = new PlayerThread;
	connect(m_pPlayerThread, SIGNAL(positionChanged(int)), SLOT(slotPositionChanged(int)));
	connect(m_pPlayerThread, SIGNAL(durationChanged(int)), SLOT(slotDurationChanged(int)));

	createWidgets();
	createLayouts();
	createActions();
	createMenus();

	m_pEqualizerDock->setWidget(m_pEqualizer);
	connect(m_pEqualizerDock, SIGNAL(visibilityChanged(bool)), m_pActionViewEqualizer, SLOT(setChecked(bool)));
	addDockWidget(Qt::BottomDockWidgetArea, m_pEqualizerDock);

	setWindowTitle(WindowTitle);
}

MainWindow::~MainWindow()
{
	unload();

	delete m_pPreferencesDialog;
	delete m_pSettings;
	delete m_pPlayerThread;
	delete m_pLoaderMPG123;
	delete m_pLoaderSNDFILE;
}

void MainWindow::createActions()
{
	m_pActionOpen = new QAction("&Open ...", this);
	m_pActionOpen->setShortcut(QKeySequence::Open);
	connect(m_pActionOpen, SIGNAL(triggered()), this, SLOT(load()));

	m_pActionClose = new QAction("&Close", this);
	m_pActionClose->setShortcut(QKeySequence::Close);
	connect(m_pActionClose, SIGNAL(triggered()), this, SLOT(unload()));

	m_pActionExit = new QAction("E&xit", this);
	m_pActionExit->setShortcut(QKeySequence::Quit);
	connect(m_pActionExit, SIGNAL(triggered()), this, SLOT(close()));

	m_pActionViewScope = new QAction("&Oscilloscope", this);
	m_pActionViewScope->setCheckable(true);
	m_pActionViewScope->setChecked(true);
	connect(m_pActionViewScope, SIGNAL(toggled(bool)), m_pOscilloscope, SLOT(setVisible(bool)));

	m_pActionViewWaveform = new QAction("&Waveform", this);
	m_pActionViewWaveform->setCheckable(true);
	m_pActionViewWaveform->setChecked(true);
	connect(m_pActionViewWaveform, SIGNAL(toggled(bool)), m_pWaveform, SLOT(setVisible(bool)));

	m_pActionViewSpectrum = new QAction("&Spectrum", this);
	m_pActionViewSpectrum->setCheckable(true);
	m_pActionViewSpectrum->setChecked(true);
	connect(m_pActionViewSpectrum, SIGNAL(toggled(bool)), m_pSpectrum, SLOT(setVisible(bool)));

	m_pActionViewEqualizer = new QAction("&Equalizer", this);
	m_pActionViewEqualizer->setCheckable(true);
	m_pActionViewEqualizer->setChecked(true);
	connect(m_pActionViewEqualizer, SIGNAL(toggled(bool)), m_pEqualizerDock, SLOT(setVisible(bool)));

	m_pActionEqualizerEnable = new QAction("&Equalizer enabled", this);
	m_pActionEqualizerEnable->setCheckable(true);
	m_pActionEqualizerEnable->setChecked(true);
	connect(m_pActionEqualizerEnable, SIGNAL(toggled(bool)), m_pPlayerThread, SLOT(setEqualizerEnabled(bool)));

	m_pActionPreferences = new QAction("&Preferences ...", this);
	m_pActionPreferences->setShortcut(QKeySequence::Preferences);
	connect(m_pActionPreferences, SIGNAL(triggered()), this, SLOT(preferences()));

	m_pActionAbout = new QAction("&About ...", this);
	connect(m_pActionAbout, SIGNAL(triggered()), this, SLOT(about()));

	m_pActionAboutQt = new QAction("About &Qt ...", this);
	connect(m_pActionAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
}

void MainWindow::createMenus()
{
	QMenu * fileMenu = menuBar()->addMenu("&File");
	fileMenu->addAction(m_pActionOpen);
	fileMenu->addAction(m_pActionClose);
	fileMenu->addSeparator();
	fileMenu->addAction(m_pActionExit);

	QMenu * viewMenu = menuBar()->addMenu("&View");
	viewMenu->addAction(m_pActionViewScope);
	viewMenu->addAction(m_pActionViewWaveform);
	viewMenu->addAction(m_pActionViewSpectrum);
	viewMenu->addAction(m_pActionViewEqualizer);

	QMenu * optionsMenu = menuBar()->addMenu("&Options");
	optionsMenu->addAction(m_pActionEqualizerEnable);
	optionsMenu->addSeparator();
	optionsMenu->addAction(m_pActionPreferences);

	QMenu * helpMenu = menuBar()->addMenu("&Help");
	helpMenu->addAction(m_pActionAbout);
	helpMenu->addAction(m_pActionAboutQt);
}

void MainWindow::createWidgets()
{
	m_pButtonOpen      = new QPushButton("Open ...");
	m_pButtonCue       = new QPushButton("Cue");
	m_pButtonPlayPause = new QPushButton("Play / Pause");
	m_pButtonExit      = new QPushButton("Exit");
	m_pLabelPosition   = new QLabel();
	m_pLabelGain       = new QLabel();
	m_pLabelSpeed      = new QLabel();
	m_pLabelTempo      = new QLabel();
	m_pLabelPitch      = new QLabel();
	m_pSliderPosition  = new QSlider(Qt::Horizontal);
	m_pSliderGain      = new RightClickableSlider(Qt::Horizontal);
	m_pSliderSpeed     = new RightClickableSlider(Qt::Horizontal);
	m_pSliderTempo     = new RightClickableSlider(Qt::Horizontal);
	m_pSliderPitch     = new RightClickableSlider(Qt::Horizontal);
	m_pMeterL          = new Meter;
	m_pMeterR          = new Meter;
	m_pOscilloscope    = new Oscilloscope;
	m_pWaveform        = new Waveform;
	m_pSpectrum        = new Spectrum;
	m_pEqualizer       = new Equalizer(m_pSettings->getEqRange());
	m_pEqualizerDock   = new QDockWidget("Equalizer");

	connect(m_pButtonOpen, SIGNAL(clicked()), SLOT(load()));
	connect(m_pButtonExit, SIGNAL(clicked()), SLOT(close()));

	connect(m_pSliderGain,  SIGNAL(valueChanged(int)), SLOT(slotGainChanged(int)));
	connect(m_pSliderSpeed, SIGNAL(valueChanged(int)), SLOT(slotSpeedChanged(int)));
	connect(m_pSliderTempo, SIGNAL(valueChanged(int)), SLOT(slotTempoChanged(int)));
	connect(m_pSliderPitch, SIGNAL(valueChanged(int)), SLOT(slotPitchChanged(int)));

	connect(m_pSliderSpeed, SIGNAL(sliderMoved(int)), SLOT(slotSpeedSliderMoved(int)));
	connect(m_pSliderTempo, SIGNAL(sliderMoved(int)), SLOT(slotTempoSliderMoved(int)));
	connect(m_pSliderPitch, SIGNAL(sliderMoved(int)), SLOT(slotPitchSliderMoved(int)));

	connect(m_pSliderGain,  SIGNAL(rightClicked()), SLOT(slotGainSliderRightClicked()));
	connect(m_pSliderSpeed, SIGNAL(rightClicked()), SLOT(slotSpeedSliderRightClicked()));
	connect(m_pSliderTempo, SIGNAL(rightClicked()), SLOT(slotTempoSliderRightClicked()));
	connect(m_pSliderPitch, SIGNAL(rightClicked()), SLOT(slotPitchSliderRightClicked()));

	setSliderRange(m_pSliderGain,  m_pSettings->getGainRange(),  SliderScaleFactorGain);
	setSliderRange(m_pSliderSpeed, m_pSettings->getSpeedRange(), SliderScaleFactorSpeed);
	setSliderRange(m_pSliderTempo, m_pSettings->getTempoRange(), SliderScaleFactorTempo);
	setSliderRange(m_pSliderPitch, m_pSettings->getPitchRange(), SliderScaleFactorPitch);

	updateLabelGain (0.0);
	updateLabelSpeed(0.0f);
	updateLabelTempo(0.0f);
	updateLabelPitch(0.0f);

	connect(m_pButtonPlayPause, SIGNAL(clicked()),  m_pPlayerThread, SLOT(playpause()));
	connect(m_pButtonCue,       SIGNAL(pressed()),  m_pPlayerThread, SLOT(cueStart()));
	connect(m_pButtonCue,       SIGNAL(released()), m_pPlayerThread, SLOT(cueStop()));

	m_pSliderPosition->setRange(0, 0);
	updateLabelPosition(0);

	connect(m_pSliderPosition, SIGNAL(sliderPressed()),  m_pPlayerThread, SLOT(seekingBegin()));
	connect(m_pSliderPosition, SIGNAL(sliderMoved(int)), m_pPlayerThread, SLOT(seekingStep(int)));
	connect(m_pSliderPosition, SIGNAL(sliderReleased()), m_pPlayerThread, SLOT(seekingEnd()));

	connect(m_pWaveform, SIGNAL(seekingBegun()),    m_pPlayerThread, SLOT(seekingBegin()));
	connect(m_pWaveform, SIGNAL(seekingMoved(int)), m_pPlayerThread, SLOT(seekingStep(int)));
	connect(m_pWaveform, SIGNAL(seekingEnded()),    m_pPlayerThread, SLOT(seekingEnd()));

	connect(m_pEqualizer, SIGNAL(changed()), SLOT(slotEqualizerChanged()));

	connect(m_pPlayerThread, SIGNAL(bufferSent(short*,int,int)), m_pOscilloscope, SLOT(setData(short*,int,int)));
	connect(m_pPlayerThread, SIGNAL(bufferSent(short*,int,int)), m_pSpectrum,     SLOT(setData(short*,int,int)));

	connect(m_pPlayerThread, SIGNAL(leftLevelsChanged (unsigned short,unsigned short)), m_pMeterL, SLOT(setValues(unsigned short,unsigned short)));
	connect(m_pPlayerThread, SIGNAL(rightLevelsChanged(unsigned short,unsigned short)), m_pMeterR, SLOT(setValues(unsigned short,unsigned short)));
}

void MainWindow::createLayouts()
{
	QHBoxLayout * buttons_layout = new QHBoxLayout;
	buttons_layout->addWidget(m_pButtonOpen);
	buttons_layout->addWidget(m_pButtonCue);
	buttons_layout->addWidget(m_pButtonPlayPause);
	buttons_layout->addWidget(m_pButtonExit);

	QGridLayout * controls_layout = new QGridLayout;
	controls_layout->addWidget(new QLabel("Position"), 0, 0);
	controls_layout->addWidget(new QLabel("Gain"),     1, 0);
	controls_layout->addWidget(new QLabel("Speed"),    2, 0);
	controls_layout->addWidget(new QLabel("Tempo"),    3, 0);
	controls_layout->addWidget(new QLabel("Pitch"),    4, 0);
	controls_layout->addWidget(m_pSliderPosition, 0, 1);
	controls_layout->addWidget(m_pSliderGain,     1, 1);
	controls_layout->addWidget(m_pSliderSpeed,    2, 1);
	controls_layout->addWidget(m_pSliderTempo,    3, 1);
	controls_layout->addWidget(m_pSliderPitch,    4, 1);
	controls_layout->addWidget(m_pLabelPosition,  0, 2, Qt::AlignRight);
	controls_layout->addWidget(m_pLabelGain,      1, 2, Qt::AlignRight);
	controls_layout->addWidget(m_pLabelSpeed,     2, 2, Qt::AlignRight);
	controls_layout->addWidget(m_pLabelTempo,     3, 2, Qt::AlignRight);
	controls_layout->addWidget(m_pLabelPitch,     4, 2, Qt::AlignRight);

	QVBoxLayout * main_layout = new QVBoxLayout;
	main_layout->addLayout(buttons_layout);
	main_layout->addLayout(controls_layout);
	main_layout->addWidget(m_pMeterL);
	main_layout->addWidget(m_pMeterR);
	main_layout->addWidget(m_pOscilloscope);
	main_layout->addWidget(m_pWaveform);
	main_layout->addWidget(m_pSpectrum);

	QWidget * central_widget = new QWidget;
	central_widget->setLayout(main_layout);

	setCentralWidget(central_widget);
}

void MainWindow::setSliderRange(QSlider * slider, int scale, int range)
{
	int slider_range = scale * range;
	slider->setRange(-slider_range, +slider_range);
}

int MainWindow::getGainRange()
{
	return m_pSettings->getGainRange();
}

void MainWindow::setGainRange(int range)
{
	m_pSettings->setGainRange(range);

	setSliderRange(m_pSliderGain, SliderScaleFactorGain, range);
}

int MainWindow::getSpeedRange()
{
	return m_pSettings->getSpeedRange();
}

void MainWindow::setSpeedRange(int range)
{
	m_pSettings->setSpeedRange(range);

	setSliderRange(m_pSliderSpeed, SliderScaleFactorSpeed, range);
}

int MainWindow::getTempoRange()
{
	return m_pSettings->getTempoRange();
}

void MainWindow::setTempoRange(int range)
{
	m_pSettings->setTempoRange(range);

	setSliderRange(m_pSliderTempo, SliderScaleFactorTempo, range);
}

int MainWindow::getPitchRange()
{
	return m_pSettings->getPitchRange();
}

void MainWindow::setPitchRange(int range)
{
	m_pSettings->setPitchRange(range);

	setSliderRange(m_pSliderPitch, SliderScaleFactorPitch, range);
}

int MainWindow::getEqRange()
{
	return m_pSettings->getEqRange();
}

void MainWindow::setEqRange(int range)
{
	m_pSettings->setEqRange(range);

	m_pEqualizer->setRange(range);
}

void MainWindow::updateLabelPosition(int value_samples)
{
	if (m_pAudioData)
	{
		m_pLabelPosition->setText(formatTime(value_samples, m_pAudioData->samplerate()) + " / " + m_strDuration);
	}
	else
	{
		m_pLabelPosition->setText("00:00.000 / 00:00.000");
	}
}

void MainWindow::updateLabelGain(double value_dB)
{
	double value_percent = 100.0 * qPow(20.0, value_dB / 10.0);
	QString s;
	s.sprintf("%+.2f dB (%.2f %%)", value_dB, value_percent);
	m_pLabelGain->setText(s);
}

void MainWindow::updateLabelSpeed(float value_percent)
{
	QString s;
	s.sprintf("%+.1f %%", value_percent);
	m_pLabelSpeed->setText(s);
}

void MainWindow::updateLabelTempo(float value_percent)
{
	QString s;
	s.sprintf("%+.1f %%", value_percent);
	m_pLabelTempo->setText(s);
}

void MainWindow::updateLabelPitch(float value_percent)
{
	QString s;
	s.sprintf("%+.1f %%", value_percent);
	m_pLabelPitch->setText(s);
}

void MainWindow::slotGainChanged(int value)
{
	double value_dB  = (double) value / (double) SliderScaleFactorGain;
	double value_lin = qPow(20.0, value_dB / 10.0);

	m_pPlayerThread->setGain(value_lin);

	m_pWaveform->setAmplification(value_lin);

	updateLabelGain(value_dB);
}

void MainWindow::slotSpeedChanged(int value)
{
	float value_percent = (float) value / (float) SliderScaleFactorSpeed;

	updateLabelSpeed(value_percent);
}

void MainWindow::slotTempoChanged(int value)
{
	float value_percent = (float) value / (float) SliderScaleFactorTempo;

	m_pPlayerThread->setTempo(value_percent / 100.0f);

	updateLabelTempo(value_percent);
}

void MainWindow::slotPitchChanged(int value)
{
	float value_percent = (float) value / (float) SliderScaleFactorPitch;

	m_pPlayerThread->setPitch(value_percent / 100.0f);

	updateLabelPitch(value_percent);
}

void MainWindow::slotSpeedSliderMoved(int value)
{
	m_pSliderTempo->setValue(value);
	m_pSliderPitch->setValue(value + m_nPitchDifference);
}

void MainWindow::slotTempoSliderMoved(int value)
{
	m_nPitchDifference = m_pSliderPitch->value() - value;
	m_pSliderSpeed->setValue(value);
}

void MainWindow::slotPitchSliderMoved(int value)
{
	m_nPitchDifference = value - m_pSliderTempo->value();
}

void MainWindow::slotGainSliderRightClicked()
{
	m_pSliderGain->setValue(0);
}

void MainWindow::slotSpeedSliderRightClicked()
{
	m_pSliderSpeed->setValue(0);
	m_pSliderTempo->setValue(0);
	m_pSliderPitch->setValue(0);
	m_nPitchDifference = 0;
}

void MainWindow::slotTempoSliderRightClicked()
{
	m_pSliderSpeed->setValue(0);
	m_pSliderTempo->setValue(0);
	m_nPitchDifference = m_pSliderPitch->value();
}

void MainWindow::slotPitchSliderRightClicked()
{
	m_pSliderPitch->setValue(0);
	m_nPitchDifference = -m_pSliderTempo->value();
}

void MainWindow::slotEqualizerChanged()
{
	m_pPlayerThread->setEqualizerBands(m_pEqualizer->getBands());
}

void MainWindow::slotPositionChanged(int position)
{
	m_pSliderPosition->setValue(position);
	m_pWaveform->setPosition(position);
	updateLabelPosition(position);
}

void MainWindow::slotDurationChanged(int duration)
{
	m_pSliderPosition->setRange(0, duration);
	m_pSliderPosition->setValue(0);

	if (m_pAudioData)
	{
		m_strDuration = formatTime(duration, m_pAudioData->samplerate());
	}
	else
	{
		m_strDuration = "00:00.000";
	}

	updateLabelPosition(0);
}

void MainWindow::load()
{
	QString filename = QFileDialog::getOpenFileName(this, "Open", m_strDirectory, FileDialogFilter);
	if (filename.length() > 0)
	{
		unload();

		QFileInfo info(filename);
		QString extension = info.suffix();
		if (extension == "mp1" || extension == "mp2" || extension == "mp3")
		{
			m_pAudioData = m_pLoaderMPG123->load(filename.toUtf8());
		}
		else
		{
			m_pAudioData = m_pLoaderSNDFILE->load(filename.toUtf8());
		}

		if (m_pAudioData)
		{
			m_pPlayerThread->setInput(m_pAudioData);

			m_pWaveform->setInput(m_pAudioData);

			m_strDirectory = info.absolutePath();
			setWindowTitle(info.fileName() + " - " + WindowTitle);
		}
		else
		{
			QMessageBox::critical(this, "Error", "Cannot load file \'" + filename + "\' !");
		}
	}
}

void MainWindow::unload()
{
	setWindowTitle(WindowTitle);

	m_pWaveform->clearInput();
	m_pPlayerThread->clearInput();

	if (m_pAudioData)
	{
		delete m_pAudioData;
		m_pAudioData = 0;
	}

	m_pSpectrum->clearData();
	m_pOscilloscope->clearData();
}

void MainWindow::preferences()
{
	m_pPreferencesDialog->show();
}

void MainWindow::about()
{
	QMessageBox::information(this, AboutTitle, AboutText);
}

QString MainWindow::formatTime(int samples, int samplerate)
{
	int miliseconds = (1000 * (samples % samplerate)) / samplerate;
	int seconds = samples / samplerate;
	QString s;
	s.sprintf("%02d:%02d.%03d", seconds / 60, seconds % 60, miliseconds);
	return s;
}
