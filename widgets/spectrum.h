#ifndef SPECTRUM_H
#define SPECTRUM_H

#include <QWidget>

#include <fftw3.h>

class Spectrum : public QWidget
{
	Q_OBJECT

public:
	explicit Spectrum(QWidget *parent = 0);
	~Spectrum();

	QSize sizeHint() const;

public slots:
	void setData(short * data, int channels, int samples);
	void clearData();

protected:
	void paintEvent(QPaintEvent *);

private:
	int            m_numsamples;
	double       * m_samples;
	double       * m_window;
	int            m_fftsize;
	fftw_complex * m_fft;
	fftw_plan      m_plan;
	bool           m_bPlanCreated;
	bool           m_bNeedToComputeFFT;

	void prepare(int /* number of input samples */);
	void cleanup();

	static void hann_window(double*, int);
	static void copy_short_to_double_array(double * dest, short * src, double * wnd, int n /* number of elements in destination */, int skip);
};

#endif // SPECTRUM_H
