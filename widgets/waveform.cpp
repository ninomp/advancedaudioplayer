#include <QPainter>
#include <QMouseEvent>

#include "waveform.h"
#include "audio/audiodata.h"

const int Waveform::ZoomLevel = 250;

Waveform::Waveform(QWidget *parent) :
    QWidget(parent)
{
	m_pData     = 0;
	m_nDataSize = 0;
	m_nDataZoom = 0;
	m_nPosition = 0;
	m_nAmplification = 1024;

	m_colorLA.setRgb(0, 255, 0);
	m_colorLB.setRgb(128, 255, 128);
	m_colorRA.setRgb(255, 0, 0);
	m_colorRB.setRgb(255, 128, 128);

	setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

QSize Waveform::sizeHint() const
{
	return QSize(100, 40);
}

bool Waveform::createData(int samples, int zoom)
{
	int n = samples / zoom;
	m_pData = new AudioLevel[n];
	if (m_pData)
	{
		m_nDataSize = n;
		m_nDataZoom = zoom;

		return true;
	}
	else  return false;
}

void Waveform::deleteData()
{
	if (m_pData)  delete m_pData;
	m_pData     = 0;
	m_nDataSize = 0;
	m_nDataZoom = 0;
}

void Waveform::setInput(const AudioData * data)
{
	if (createData(data->samples(), ZoomLevel))
	{
		m_nChannels = data->channels();
		m_nPosition = 0;

		if (data->channels() == 1)
		{
			short * p = data->data();
			for (int i = 0; i < m_nDataSize; i++)
			{
				measureAudioLevelsMono(m_pData + i, p, m_nDataZoom);
				p += m_nDataZoom;
			}
		}
		else if (data->channels() == 2)
		{
			StereoSampleBlock * p = (StereoSampleBlock*) data->data();
			for (int i = 0; i < m_nDataSize; i++)
			{
				measureAudioLevelsStereo(m_pData + i, p, m_nDataZoom);
				p += m_nDataZoom;
			}
		}
	}

	update();
}

void Waveform::clearInput()
{
	deleteData();
	update();
}

void Waveform::setPosition(int position)
{
	if (m_nDataZoom > 0)  m_nPosition = position / m_nDataZoom;
	else                  m_nPosition = position / ZoomLevel;

	if (m_nPosition >= m_nDataSize)  m_nPosition = m_nDataSize - 1;

	update();
}

void Waveform::setAmplification(double amplification)
{
	m_nAmplification = 1024.0 * amplification;
	update();
}

void Waveform::mousePressEvent(QMouseEvent * event)
{
	if (event->buttons() & Qt::LeftButton)
	{
		m_nStartX   = event->x();
		m_nStartPos = m_nPosition;
		emit seekingBegun();
	}
}

void Waveform::mouseMoveEvent(QMouseEvent * event)
{
	if (event->buttons() & Qt::LeftButton)
	{
		m_nPosition = m_nStartPos + (m_nStartX - event->x());
		emit seekingMoved(m_nPosition * m_nDataZoom);
		update();
	}
}

void Waveform::mouseReleaseEvent(QMouseEvent *)
{
	emit seekingEnded();
}

inline int amplify(short value, long long amplificator)
{
	long long temp = value * amplificator;
	return temp >> 25;
}

void Waveform::paintEvent(QPaintEvent *)
{
	QPainter painter(this);

	int w  = width();
	int h  = height();
	int x0 = w / 2;
	painter.drawLine(x0, 0, x0, h);

	if (m_pData)
	{
		int data_begin = qMax(0, m_nPosition - x0);
		int x_start    = qMax(0, x0 - m_nPosition);
		int x_end      = qMin(w, x0 + m_nDataSize - m_nPosition);
		AudioLevel * p = m_pData + data_begin;

		if (m_nChannels == 1)
		{
			long amp = m_nAmplification * h;
			painter.translate(0, height());
			painter.scale(1, -1);
			for (int x = x_start; x < x_end; x++)
			{
				int rms  = amplify(p->left_rms,  amp);
				int peak = amplify(p->left_peak, amp);
				painter.setPen(m_colorLA);
				painter.drawLine(x, 0, x, rms);
				painter.setPen(m_colorLB);
				painter.drawLine(x, rms, x, peak);
				p++;
			}
		}
		else if (m_nChannels == 2)
		{
			long amp = (m_nAmplification * h) / 2;
			painter.translate(0, h / 2);
			for (int x = x_start; x < x_end; x++)
			{
				int rmsL  = amplify(p->left_rms,   amp);
				int peakL = amplify(p->left_peak,  amp);
				int rmsR  = amplify(p->right_rms,  amp);
				int peakR = amplify(p->right_peak, amp);
				painter.setPen(m_colorLA);
				painter.drawLine(x, 0, x, -rmsL);
				painter.setPen(m_colorLB);
				painter.drawLine(x, -rmsL, x, -peakL);
				painter.setPen(m_colorRA);
				painter.drawLine(x, 0, x, +rmsR);
				painter.setPen(m_colorRB);
				painter.drawLine(x, +rmsR, x, peakR);
				p++;
			}
		}
	}
}
