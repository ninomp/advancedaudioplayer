#include <QtCore/qmath.h>
#include <QPainter>

#include "spectrum.h"

Spectrum::Spectrum(QWidget *parent) :
    QWidget(parent)
{
	m_numsamples   = 0;
	m_samples      = 0;
	m_window       = 0;
	m_fftsize      = 0;
	m_fft          = 0;
	m_bPlanCreated = false;

	setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

Spectrum::~Spectrum()
{
	cleanup();
}

QSize Spectrum::sizeHint() const
{
	return QSize(100, 40);
}

inline double fftw_complex_magnitude(fftw_complex complex)
{
	return qSqrt(complex[0] * complex[0] + complex[1] * complex[1]);
}

void Spectrum::paintEvent(QPaintEvent *)
{
	QPainter painter(this);

	if (m_bPlanCreated)
	{
		if (m_bNeedToComputeFFT)
		{
			fftw_execute(m_plan);  // Do compute FFT
			m_bNeedToComputeFFT = false;  // Clear flag
		}

		int h = height();
		painter.translate(0, h);
		painter.scale(1.0, -1.0);

		if (m_fft)
		{
			for (int x = 0; x < m_fftsize; x++)
			{
				int y = fftw_complex_magnitude(m_fft[x]);
				painter.drawLine(x, 0, x, y);
			}
		}
	}
}

void Spectrum::setData(short *data, int channels, int samples)
{
	if (!m_bPlanCreated)  prepare(samples);

	// Copy data to internal buffer
	copy_short_to_double_array(m_samples, data, m_window, m_numsamples, channels);

	// Set flag
	m_bNeedToComputeFFT = true;

	// Request repaint
	update();
}

void Spectrum::clearData()
{
	cleanup();
	update();
}

void Spectrum::prepare(int n /* samples */)
{
	// Delete everything that was previously created
	cleanup();

	m_numsamples   = n;
	m_samples      = (double*) fftw_malloc(sizeof(double) * n);
	m_window       = (double*) fftw_malloc(sizeof(double) * n);  // NOTE: Can be replaced with 'new double [n];'
	hann_window(m_window, n);

	m_fftsize      = n / 2 + 1;
	m_fft          = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * m_fftsize);
	m_plan         = fftw_plan_dft_r2c_1d(n, m_samples, m_fft, FFTW_ESTIMATE);
	m_bPlanCreated = true;
}

void Spectrum::cleanup()
{
	if (m_bPlanCreated)  fftw_destroy_plan(m_plan);
	if (m_fft)           fftw_free(m_fft);
	if (m_window)        fftw_free(m_window);  // NOTE: Can be replaced with 'delete m_window;'
	if (m_samples)       fftw_free(m_samples);

	m_numsamples   = 0;
	m_samples      = 0;
	m_window       = 0;
	m_fftsize      = 0;
	m_fft          = 0;
	m_bPlanCreated = false;
}

void Spectrum::hann_window(double * p, int n)
{
	for (int i = 0; i < n; i++)
	{
		p[i] = 1.0 - qCos((2.0 * M_PI * i) / n);
	}
}

void Spectrum::copy_short_to_double_array(double * dest, short * src, double * wnd, int n /* number of elements in destination */, int skip)
{
	while (n-- > 0)
	{
		*dest++ = (*wnd++ * *src) / 32767.0;
		src += skip;
	}
}
