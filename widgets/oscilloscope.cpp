#include <QPainter>

#include "oscilloscope.h"

Oscilloscope::Oscilloscope(QWidget *parent) :
    QWidget(parent)
{
	m_pData     = 0;
	m_nChannels = 0;
	m_nSamples  = 0;

	setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

QSize Oscilloscope::sizeHint() const
{
	return QSize(100, 40);
}

void Oscilloscope::paintEvent(QPaintEvent *)
{
	QPainter painter(this);

	if (m_pData)
	{
		short * p = m_pData;

		if (m_nChannels == 1)
		{
			int y0 = height() / 2;

			painter.translate(0, y0);

			int n = qMin(width(), m_nSamples);

			int lasty = 0;

			for (int x = 0; x < n; x++)
			{
				int y = (*p++ * y0) >> 15;
				painter.drawLine(x - 1, lasty, x, y);
				lasty = y;
			}
		}
		else if (m_nChannels == 2)
		{
			int yScale  = height() / 4;
			int offsetL = (height() * 1) / 4;
			int offsetR = (height() * 3) / 4;

			int lastL = offsetL + ((*p++ * yScale) >> 15);
			int lastR = offsetR + ((*p++ * yScale) >> 15);

			int n = qMin(m_nSamples, width());
			for (int x = 0; x < n; x++)
			{
				int l = offsetL + ((*p++ * yScale) >> 15);
				int r = offsetR + ((*p++ * yScale) >> 15);
				painter.drawLine(x - 1, lastL, x, l);
				painter.drawLine(x - 1, lastR, x, r);
				lastL = l;
				lastR = r;
			}
		}
	}
	else
	{
		int w  = width();
		int h4 = height() / 4;

		painter.drawLine(0, 1 * h4, w, 1 * h4);
		//painter.drawLine(0, 2 * h4, w, 2 * h4);
		painter.drawLine(0, 3 * h4, w, 3 * h4);
	}
}

void Oscilloscope::setData(short * data, int channels, int samples)
{
	m_pData     = data;
	m_nChannels = channels;
	m_nSamples  = samples;
	update();
}

void Oscilloscope::clearData()
{
	m_pData     = NULL;
	m_nChannels = 0;
	m_nSamples  = 0;
	update();
}
