#ifndef WAVEFORM_H
#define WAVEFORM_H

#include <QWidget>

// Forward declarations of internal classes and structures
class  AudioData;
struct AudioLevel;

class Waveform : public QWidget
{
	Q_OBJECT

public:
	explicit Waveform(QWidget *parent = 0);

	void setInput(const AudioData *);
	void clearInput();

	QSize sizeHint() const;

signals:
	void seekingBegun();
	void seekingMoved(int);
	void seekingEnded();

public slots:
	void setPosition(int position);
	void setAmplification(double);

protected:
	void mousePressEvent(QMouseEvent *);
	void mouseMoveEvent(QMouseEvent *);
	void mouseReleaseEvent(QMouseEvent *);
	void paintEvent(QPaintEvent *);

private:
	static const int ZoomLevel;  // How many audio samples in one waveform element

	bool createData(int samples, int zoom);
	void deleteData();

	AudioLevel * m_pData;
	int m_nDataSize;
	int m_nDataZoom;
	int m_nChannels;
	int m_nPosition;
	int m_nAmplification;
	int m_nStartPos;
	int m_nStartX;

	QColor m_colorLA;
	QColor m_colorLB;
	QColor m_colorRA;
	QColor m_colorRB;
};

#endif // WAVEFORM_H
