#include <QPainter>

#include "meter.h"

Meter::Meter(QWidget *parent) :
	QWidget(parent), m_colorRMS(Qt::green), m_colorPeak(Qt::red), m_colorBackground(Qt::black)
{
	m_rms = m_peak = 0;

	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
}

QSize Meter::sizeHint() const
{
	return QSize(80, 8);
}

void Meter::setValues(unsigned short rms, unsigned short peak)
{
	m_rms  = rms;
	m_peak = peak;

	update();
}

void Meter::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	int w = width();
	int h = height();
	int rms  = (m_rms  * w) >> 15;
	int peak = (m_peak * w) >> 15;
	painter.fillRect(0,    0, rms,         h, m_colorRMS);
	painter.fillRect(rms,  0, peak - rms,  h, m_colorPeak);
	painter.fillRect(peak, 0, w    - peak, h, m_colorBackground);
}
