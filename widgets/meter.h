#ifndef METER_H
#define METER_H

#include <QWidget>

class Meter : public QWidget
{
	Q_OBJECT

public:
	explicit Meter(QWidget *parent = 0);

	QSize sizeHint() const;

public slots:
	void setValues(unsigned short rms, unsigned short peak);

protected:
	void paintEvent(QPaintEvent *);

private:
	unsigned short m_rms;
	unsigned short m_peak;

	QColor m_colorRMS;
	QColor m_colorPeak;
	QColor m_colorBackground;
};

#endif // METER_H
