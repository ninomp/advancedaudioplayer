#include <QGridLayout>
#include <QLabel>

#include "equalizer.h"
#include "rightclickableslider.h"

Equalizer::Equalizer(int range, QWidget *parent) :
    QWidget(parent)
{
	m_bands << EqBand(20);
	m_bands << EqBand(50);
	m_bands << EqBand(100);
	m_bands << EqBand(200);
	m_bands << EqBand(500);
	m_bands << EqBand(1000);
	m_bands << EqBand(2000);
	m_bands << EqBand(5000);
	m_bands << EqBand(10000);
	m_bands << EqBand(20000);

	createControls(range);
}

EqualizerBands Equalizer::getBands()
{
	return m_bands;
}

void Equalizer::setRange(int range_dB)
{
	int range = 10 * range_dB;

	for (int i = 0; i < m_sliders.count(); i++)
	{
		m_sliders[i]->setRange(-range, +range);
	}
}

void Equalizer::createControls(int range_dB)
{
	QGridLayout * layout = new QGridLayout();
	setLayout(layout);

	for (int i = 0; i < m_bands.count(); i++)
	{
		EqBand                 band    = m_bands[i];
		QLabel               * pLabel  = new QLabel;
		RightClickableSlider * pSlider = new RightClickableSlider(Qt::Vertical);

		pLabel->setAlignment(Qt::AlignCenter);

		int range = 10 * range_dB;
		pSlider->setRange(-range, +range);
		pSlider->setValue(band.getLogarithmicGain() * 10.0);
		pSlider->setMinimumHeight(100);

		connect(pSlider, SIGNAL(valueChanged(int)), SLOT(sliderValueChanged(int)));
		connect(pSlider, SIGNAL(rightClicked()),    SLOT(sliderRightClicked()));

		layout->addWidget(pLabel,  0, i);
		layout->addWidget(pSlider, 1, i);

		m_labels  << pLabel;
		m_sliders << pSlider;

		updateLabel(i);
	}
}

void Equalizer::updateLabel(int index)
{
	EqBand band = m_bands[index];

	QString s;
	if (band.frequency < 1000)
		s.sprintf("%d Hz\n%+.1f dB", band.frequency, band.getLogarithmicGain());
	else
		s.sprintf("%d kHz\n%+.1f dB", band.frequency / 1000, band.getLogarithmicGain());

	m_labels[index]->setText(s);
}

void Equalizer::sliderValueChanged(int value)
{
	RightClickableSlider * slider = qobject_cast<RightClickableSlider*>(sender());
	int index = m_sliders.indexOf(slider);

	if (index >= 0)
	{
		m_bands[index].setGain_dB(value / 10.0);

		updateLabel(index);

		emit changed();
	}
}

void Equalizer::sliderRightClicked()
{
	RightClickableSlider * slider = qobject_cast<RightClickableSlider*>(sender());
	int index = m_sliders.indexOf(slider);

	if (index >= 0)
	{
		slider->setValue(0);
	}
}
