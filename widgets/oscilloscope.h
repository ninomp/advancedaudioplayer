#ifndef OSCILLOSCOPE_H
#define OSCILLOSCOPE_H

#include <QWidget>

class Oscilloscope : public QWidget
{
	Q_OBJECT

public:
	explicit Oscilloscope(QWidget *parent = 0);

	QSize sizeHint() const;

protected:
	void paintEvent(QPaintEvent *);

public slots:
	void setData(short * data, int channels, int samples);
	void clearData();

private:
	short * m_pData;
	int m_nChannels;
	int m_nSamples;
};

#endif // OSCILLOSCOPE_H
