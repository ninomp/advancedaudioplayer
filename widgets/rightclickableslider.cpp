#include <QMouseEvent>

#include "rightclickableslider.h"

RightClickableSlider::RightClickableSlider(QWidget *parent) :
	QSlider(parent)
{
}

RightClickableSlider::RightClickableSlider(Qt::Orientation orientation, QWidget *parent) :
	QSlider(orientation, parent)
{
}

void RightClickableSlider::mouseReleaseEvent(QMouseEvent *ev)
{
	QSlider::mouseReleaseEvent(ev);

	if (ev->button() == Qt::RightButton)
	{
		emit rightClicked();
	}
}
