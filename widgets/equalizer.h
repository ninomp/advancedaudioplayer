#ifndef EQUALIZER_H
#define EQUALIZER_H

#include <QWidget>

#include "equalizerbands.h"

class QLabel;
class RightClickableSlider;

class Equalizer : public QWidget
{
	Q_OBJECT

public:
	explicit Equalizer(int range_dB = 5, QWidget *parent = 0);

	EqualizerBands getBands();

	void setRange(int);

signals:
	void changed();

private slots:
	void sliderValueChanged(int);
	void sliderRightClicked();

private:
	EqualizerBands m_bands;

	QList<QLabel               *> m_labels;
	QList<RightClickableSlider *> m_sliders;

	void createControls(int range);
	void updateLabel(int index);
};

#endif // EQUALIZER_H
