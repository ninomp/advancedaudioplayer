#ifndef RIGHTCLICKABLESLIDER_H
#define RIGHTCLICKABLESLIDER_H

#include <QSlider>

class RightClickableSlider : public QSlider
{
	Q_OBJECT

public:
	explicit RightClickableSlider(QWidget *parent = 0);
	explicit RightClickableSlider(Qt::Orientation, QWidget *parent = 0);

protected:
	virtual void mouseReleaseEvent(QMouseEvent *ev);

signals:
	void rightClicked();
};

#endif // RIGHTCLICKABLESLIDER_H
