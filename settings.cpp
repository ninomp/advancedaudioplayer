#include "settings.h"

Settings::Settings(QString filename)
{
	m_pSettings = new QSettings(filename, QSettings::IniFormat);
}

Settings::~Settings()
{
	delete m_pSettings;
}
