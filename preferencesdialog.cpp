#include <QSpinBox>
#include <QGroupBox>
#include <QDialogButtonBox>
#include <QLayout>
#include <QFormLayout>

#include "preferencesdialog.h"
#include "mainwindow.h"

const QString PreferencesDialog::WindowTitle = "Preferences";

PreferencesDialog::PreferencesDialog(MainWindow *parent) : QDialog(parent)
{
	m_pMainWindow = parent;

	createWidgets();
	createLayouts();

	setWindowTitle(WindowTitle);
}

PreferencesDialog::~PreferencesDialog()
{
}

void PreferencesDialog::createWidgets()
{
	m_pRangeGain = new QSpinBox;
	m_pRangeGain->setRange(1, 10);
	m_pRangeGain->setValue(m_pMainWindow->getGainRange());
	connect(m_pRangeGain, SIGNAL(valueChanged(int)), SLOT(rangeGainChanged(int)));

	m_pRangeSpeed = new QSpinBox;
	m_pRangeSpeed->setRange(1, 50);
	m_pRangeSpeed->setValue(m_pMainWindow->getSpeedRange());
	connect(m_pRangeSpeed, SIGNAL(valueChanged(int)), SLOT(rangeSpeedChanged(int)));

	m_pRangeTempo = new QSpinBox;
	m_pRangeTempo->setRange(1, 50);
	m_pRangeTempo->setValue(m_pMainWindow->getTempoRange());
	connect(m_pRangeTempo, SIGNAL(valueChanged(int)), SLOT(rangeTempoChanged(int)));

	m_pRangePitch = new QSpinBox;
	m_pRangePitch->setRange(1, 70);
	m_pRangePitch->setValue(m_pMainWindow->getPitchRange());
	connect(m_pRangePitch, SIGNAL(valueChanged(int)), SLOT(rangePitchChanged(int)));

	m_pRangeEq = new QSpinBox;
	m_pRangeEq->setRange(1, 10);
	m_pRangeEq->setValue(m_pMainWindow->getEqRange());
	connect(m_pRangeEq, SIGNAL(valueChanged(int)), SLOT(rangeEqChanged(int)));
}

void PreferencesDialog::createLayouts()
{
	QGroupBox * group_ranges = new QGroupBox("Ranges");
	QFormLayout * layout_ranges = new QFormLayout(group_ranges);
	layout_ranges->addRow("Gain range (dB)",      m_pRangeGain);
	layout_ranges->addRow("Speed range (%)",      m_pRangeSpeed);
	layout_ranges->addRow("Tempo range (%)",      m_pRangeTempo);
	layout_ranges->addRow("Pitch range (%)",      m_pRangePitch);
	layout_ranges->addRow("Equalizer range (dB)", m_pRangeEq);

	QDialogButtonBox * button_box = new QDialogButtonBox(QDialogButtonBox::Close);
	connect(button_box, SIGNAL(rejected()), SLOT(close()));

	QVBoxLayout * main_layout = new QVBoxLayout(this);
	main_layout->addWidget(group_ranges);
	main_layout->addWidget(button_box);
}

void PreferencesDialog::rangeGainChanged(int range)
{
	m_pMainWindow->setGainRange(range);
}

void PreferencesDialog::rangeSpeedChanged(int range)
{
	m_pMainWindow->setSpeedRange(range);
}

void PreferencesDialog::rangeTempoChanged(int range)
{
	m_pMainWindow->setTempoRange(range);
}

void PreferencesDialog::rangePitchChanged(int range)
{
	m_pMainWindow->setPitchRange(range);
}

void PreferencesDialog::rangeEqChanged(int range)
{
	m_pMainWindow->setEqRange(range);
}
