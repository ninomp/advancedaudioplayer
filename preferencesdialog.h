#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include <QDialog>

class QSpinBox;

class MainWindow;

class PreferencesDialog : public QDialog
{
	Q_OBJECT

public:
	explicit PreferencesDialog(MainWindow *parent = 0);
	~PreferencesDialog();

private slots:
	void rangeGainChanged(int);
	void rangeSpeedChanged(int);
	void rangeTempoChanged(int);
	void rangePitchChanged(int);
	void rangeEqChanged(int);

private:
	// Helper functions
	void createWidgets();
	void createLayouts();

	// Constants
	static const QString WindowTitle;

	// Pointer to Main window instance
	MainWindow * m_pMainWindow;

	// Widgets
	QSpinBox * m_pRangeGain;
	QSpinBox * m_pRangeSpeed;
	QSpinBox * m_pRangeTempo;
	QSpinBox * m_pRangePitch;
	QSpinBox * m_pRangeEq;
};

#endif // PREFERENCESDIALOG_H
