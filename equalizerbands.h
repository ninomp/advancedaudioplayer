#ifndef EQUALIZERBANDS_H
#define EQUALIZERBANDS_H

#include <QtCore/qmath.h>
#include <QList>

class EqBand
{
public:
	int frequency;

	EqBand(int freq)
	{
		frequency = freq;
		gain_dB   = 0.0;
		gain_lin  = 1.0;
	}

	EqBand(int freq, double initialGain_dB)
	{
		frequency = freq;
		setGain_dB(initialGain_dB);
	}

	double getLogarithmicGain()
	{
		return gain_dB;
	}

	double getLinearGain()
	{
		return gain_lin;
	}

	void setGain_dB(double dB)
	{
		gain_dB  = dB;
		gain_lin = pow(20.0, dB / 10.0);
	}

	void setGain_Linear(double linear_gain)
	{
		gain_dB  = 20.0 * log10(linear_gain);
		gain_lin = linear_gain;
	}

	/*QString frequencyString()
	{
		return QString::number(frequency);
	}*/

private:
	double gain_dB;
	double gain_lin;
};

typedef QList<EqBand> EqualizerBands;

#endif // EQUALIZERBANDS_H
