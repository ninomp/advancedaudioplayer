#include "audiofilter.h"
#include "audiodata.h"

AudioFilter::AudioFilter()
{
	m_bufferSize    = 0;
	m_bufferLeft    = NULL;
	m_bufferRight   = NULL;
	m_spectrumSize  = 0;
	m_coeficients   = NULL;
	m_spectrumLeft  = NULL;
	m_spectrumRight = NULL;
	m_plansCreated  = false;
}

AudioFilter::~AudioFilter()
{
	cleanup();
}

void AudioFilter::prepare(int n)
{
	// Delete everything previously created
	cleanup();

	// Initialize
	m_bufferSize       = n;
	m_spectrumSize     = n / 2 + 1;

	// Create buffers
	m_coeficients      = new double[m_spectrumSize];
	m_bufferLeft       = (double*)       fftw_malloc(sizeof(double) * n);
	m_bufferRight      = (double*)       fftw_malloc(sizeof(double) * n);
	m_spectrumLeft     = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * m_spectrumSize);
	m_spectrumRight    = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * m_spectrumSize);

	// Create plans
	m_planForwardLeft  = fftw_plan_dft_r2c_1d(n, m_bufferLeft, m_spectrumLeft, FFTW_ESTIMATE);
	m_planInverseLeft  = fftw_plan_dft_c2r_1d(n, m_spectrumLeft, m_bufferLeft, FFTW_ESTIMATE);
	m_planForwardRight = fftw_plan_dft_r2c_1d(n, m_bufferRight, m_spectrumRight, FFTW_ESTIMATE);
	m_planInverseRight = fftw_plan_dft_c2r_1d(n, m_spectrumRight, m_bufferRight, FFTW_ESTIMATE);

	// Set flag so we know plans are created
	m_plansCreated = true;

	// Initialize coeficients
	initCoeficients();
}

void AudioFilter::cleanup()
{
	// Delete plans if they are created
	if (m_plansCreated)
	{
		fftw_destroy_plan(m_planForwardLeft);
		fftw_destroy_plan(m_planInverseLeft);
		fftw_destroy_plan(m_planForwardRight);
		fftw_destroy_plan(m_planInverseRight);
	}

	// Delete buffers
	if (m_coeficients)    delete m_coeficients;
	if (m_bufferLeft)     fftw_free(m_bufferLeft);
	if (m_bufferRight)    fftw_free(m_bufferRight);
	if (m_spectrumLeft)   fftw_free(m_spectrumLeft);
	if (m_spectrumRight)  fftw_free(m_spectrumRight);

	// Reset attributes
	m_bufferSize    = 0;
	m_bufferLeft    = NULL;
	m_bufferRight   = NULL;
	m_spectrumSize  = 0;
	m_coeficients   = NULL;
	m_spectrumLeft  = NULL;
	m_spectrumRight = NULL;
	m_plansCreated  = false;
}

void AudioFilter::filterMono(short * data)
{
	copy_short_to_double_array(data, m_bufferLeft, m_bufferSize);

	fftw_execute(m_planForwardLeft);

	multiply_point_by_point(m_spectrumLeft, m_coeficients, m_spectrumSize);

	fftw_execute(m_planInverseLeft);

	copy_double_to_short_array(m_bufferLeft, data, m_bufferSize, m_bufferSize);
}

void AudioFilter::filterStereo(StereoSampleBlock * data)
{
	deinterleave(data, m_bufferLeft, m_bufferRight, m_bufferSize);

	fftw_execute(m_planForwardLeft);
	fftw_execute(m_planForwardRight);

	multiply_point_by_point(m_spectrumLeft,  m_coeficients, m_spectrumSize);
	multiply_point_by_point(m_spectrumRight, m_coeficients, m_spectrumSize);

	fftw_execute(m_planInverseLeft);
	fftw_execute(m_planInverseRight);

	interleave(m_bufferLeft, m_bufferRight, data, m_bufferSize, m_bufferSize);
}

// Interpolate coefficients from equalizer bands
void AudioFilter::set(EqualizerBands bands, int samplerate)
{
	int nyquist_frequency = samplerate / 2;

	EqualizerBands::iterator it = bands.begin();

	if (it == bands.end())
	{
		// Empty bands list
		initCoeficients();
		return;
	}

	EqBand first = EqBand(0);
	EqBand last = EqBand(nyquist_frequency);

	EqBand a = first;
	EqBand b = *it;
	int freq_delta = b.frequency - a.frequency;

	for (int i = 0; i < m_spectrumSize; i++)
	{
		int freq = (i * nyquist_frequency) / m_spectrumSize;

		if (b.frequency < nyquist_frequency)
		{
			while (freq > b.frequency)
			{
				a = b;
				it++;
				if (it == bands.end())
				{
					b = last;
					break;
				}
				else  b = *it;
			}

			freq_delta = b.frequency - a.frequency;
		}

		// Interpolate between 'a' and 'b'
		m_coeficients[i] = (a.getLinearGain() * (b.frequency - freq)) / freq_delta + (b.getLinearGain() * (freq - a.frequency)) / freq_delta;
	}
}

void AudioFilter::initCoeficients()
{
	for (int i = 0; i < m_spectrumSize; i++)
		m_coeficients[i] = 1.0;
}

inline short clipDoubleToShort(double value)
{
	if      (value < -32768.0)  return -32768;
	else if (value > +32767.0)  return +32767;
	else                        return  value;
}

void AudioFilter::copy_short_to_double_array(short * src, double * dest, int n)
{
	for (int i = 0; i < n; i++)
	{
		dest[i] = src[i];
	}
}

void AudioFilter::copy_double_to_short_array(double * src, short * dest, int n, double divisor)
{
	for (int i = 0; i < n; i++)
	{
		dest[i] = clipDoubleToShort(src[i] / divisor);
	}
}

void AudioFilter::deinterleave(StereoSampleBlock * src, double * dest_left, double * dest_right, int n /* blocks */)
{
	for (int i = 0; i < n; i++)
	{
		dest_left[i]  = src[i].l;
		dest_right[i] = src[i].r;
	}
}

void AudioFilter::interleave(double * src_left, double * src_right, StereoSampleBlock * dest, int n /* blocks */, double divisor)
{
	for (int i = 0; i < n; i++)
	{
		dest[i].l = clipDoubleToShort(src_left [i] / divisor);
		dest[i].r = clipDoubleToShort(src_right[i] / divisor);
	}
}

// Point-by-point in-place multiplication
void AudioFilter::multiply_point_by_point(fftw_complex * p, double * q, int n)
{
	for (int i = 0; i < n; i++)
	{
		p[i][0] *= q[i];
		p[i][1] *= q[i];
	}
}
