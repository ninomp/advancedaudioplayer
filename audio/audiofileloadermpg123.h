#ifndef AUDIOFILELOADERMPG123_H
#define AUDIOFILELOADERMPG123_H

#include "audiofileloader.h"

class AudioFileLoaderMPG123 : public AudioFileLoader
{
public:
	AudioFileLoaderMPG123();
	virtual ~AudioFileLoaderMPG123();

	AudioData * load(const char *);

private:
	static const int LoadBlockSize;
};

#endif // AUDIOFILELOADERMPG123_H
