#include <sndfile.h>

#include "audiofileloadersndfile.h"

AudioData * AudioFileLoaderSNDFILE::load(const char * filename)
{
	SF_INFO sfinfo;
	SNDFILE * file = sf_open(filename, SFM_READ, &sfinfo);
	if (file)
	{
		if (sfinfo.channels == 1 || sfinfo.channels == 2)
		{
			AudioData * audio_data = AudioData::create(sfinfo.channels, sfinfo.samplerate, sfinfo.frames);
			if (audio_data)
			{
				sf_count_t frames_read = sf_readf_short(file, audio_data->data(), audio_data->samples());

				sf_close(file);

				if (frames_read > 0)
				{
					// Successfully loaded file
					return audio_data;
				}
				else
				{
					delete audio_data;
				}
			}
			else
			{
				sf_close(file);
			}
		}
		else
		{
			sf_close(file);
		}
	}

	// Failed to load file
	return 0;
}
