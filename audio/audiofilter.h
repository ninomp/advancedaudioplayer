#ifndef AUDIOFILTER_H
#define AUDIOFILTER_H

#include <fftw3.h>

#include "equalizerbands.h"

struct StereoSampleBlock;

class AudioFilter
{
public:
	AudioFilter();
	~AudioFilter();

	void prepare(int buffer_size);
	void cleanup();

	void filterMono(short*);
	void filterStereo(StereoSampleBlock*);

	void set(EqualizerBands, int samplingrate);

private:
	int            m_bufferSize;
	double       * m_bufferLeft;
	double       * m_bufferRight;
	int            m_spectrumSize;
	double       * m_coeficients;
	fftw_complex * m_spectrumLeft;
	fftw_complex * m_spectrumRight;
	fftw_plan      m_planForwardLeft;
	fftw_plan      m_planInverseLeft;
	fftw_plan      m_planForwardRight;
	fftw_plan      m_planInverseRight;
	bool           m_plansCreated;

	void initCoeficients();

	static void copy_short_to_double_array(short*,  double*, int);
	static void copy_double_to_short_array(double*, short*,  int, double);
	static void deinterleave(StereoSampleBlock*, double*, double*, int);
	static void interleave(double*, double*, StereoSampleBlock*, int, double);
	static void multiply_point_by_point(fftw_complex*, double*, int);
};

#endif // AUDIOFILTER_H
