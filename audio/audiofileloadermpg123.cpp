// WINFIX for libmpg123
#if defined(_MSC_VER)
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

#include <mpg123.h>

#include "audiofileloadermpg123.h"

const int AudioFileLoaderMPG123::LoadBlockSize = 10000;

AudioFileLoaderMPG123::AudioFileLoaderMPG123()
{
	mpg123_init();
}

AudioFileLoaderMPG123::~AudioFileLoaderMPG123()
{
	mpg123_exit();
}

AudioData * AudioFileLoaderMPG123::load(const char * filename)
{
	mpg123_handle * m = mpg123_new(NULL, NULL);
	if (m == 0)
	{
		return 0;
	}

	if (mpg123_open(m, filename) != MPG123_OK)
	{
		mpg123_delete(m);
		return 0;
	}

	mpg123_scan(m);

	long rate;
	int channels;
	int encoding;
	if (mpg123_getformat(m, &rate, &channels, &encoding) != MPG123_OK)
	{
		mpg123_close(m);
		mpg123_delete(m);
		return 0;
	}

	if (encoding != MPG123_ENC_SIGNED_16)  // Check encoding because we only support 16-bit signed integer
	{
		mpg123_close(m);
		mpg123_delete(m);
		return 0;
	}

	long long samples = mpg123_length(m);
	if (samples == MPG123_ERR)
	{
		mpg123_close(m);
		mpg123_delete(m);
		return 0;
	}

	AudioData * audio_data = AudioData::create(channels, rate, samples);
	if (audio_data == 0)
	{
		mpg123_close(m);
		mpg123_delete(m);
		return 0;
	}

	unsigned char * ptr = (unsigned char*) audio_data->data();
	size_t done = 0;
	while (mpg123_read(m, ptr, LoadBlockSize, &done) == MPG123_OK)
	{
		ptr += done;
	}

	mpg123_close(m);

	mpg123_delete(m);

	return audio_data;
}
