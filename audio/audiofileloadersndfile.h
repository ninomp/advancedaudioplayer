#ifndef AUDIOFILELOADERSNDFILE_H
#define AUDIOFILELOADERSNDFILE_H

#include "audiofileloader.h"

class AudioFileLoaderSNDFILE : public AudioFileLoader
{
public:
	AudioData * load(const char *);
};

#endif // AUDIOFILELOADERSNDFILE_H
