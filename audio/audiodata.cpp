#include <QtCore/qmath.h>

#include <string.h>

#include "audiodata.h"

AudioData * AudioData::create(int channels, int samplerate, int sample_blocks)
{
	AudioData * p = new AudioData;
	if (p->createData(channels, samplerate, sample_blocks))
	{
		return p;
	}
	else
	{
		delete p;
		return 0;
	}
}

AudioData::AudioData()
{
	m_nChannels   = 0;
	m_nSamplerate = 0;
	m_nSamples    = 0;
	m_pData       = 0;
}

AudioData::~AudioData()
{
	deleteData();
}

bool AudioData::createData(int channels, int samplerate, int sample_blocks)
{
	m_pData = new short[channels * sample_blocks];
	if (m_pData)
	{
		m_nChannels   = channels;
		m_nSamplerate = samplerate;
		m_nSamples    = sample_blocks;

		return true;
	}
	else  return false;
}

void AudioData::deleteData()
{
	if (m_pData)  delete m_pData;
	m_nChannels   = 0;
	m_nSamplerate = 0;
	m_nSamples    = 0;
	m_pData       = 0;
}

inline short applyGainWithLimit(short sample_value, int gain /* / 1024 */)
{
	int temp = (sample_value * gain) >> 10;
	if      (temp > +32767)  return +32767;
	else if (temp < -32768)  return -32768;
	else                     return temp;
}

void copyWithGain(short * dest, int dest_len, short * src, int src_len, int channels, int gain /* / 1024 */)
{
	int len = qMin(dest_len, src_len);

	if (gain == 1024)
	{
		memcpy(dest, src, sizeof(short) * channels * len);
	}
	else
	{
		for (int i = 0; i < channels * len; i++)
		{
			dest[i] = applyGainWithLimit(src[i], gain);
		}
	}
}

void resampleWithGainMono(short * dest, int dest_len, short * src, int src_len, int rate, int gain)
{
	for (int i = 0; i < dest_len; i++)
	{
		int src_index = (i * rate) >> 10;
		if (src_index < src_len)
		{
			if (gain == 1024)
			{
				dest[i] = src[src_index];
			}
			else
			{
				dest[i] = applyGainWithLimit(src[src_index], gain);
			}
		}
		else
		{
			dest[i] = 0;
		}
	}
}

void resampleWithGainStereo(StereoSampleBlock * dest, int dest_len, StereoSampleBlock * src, int src_len, int rate, int gain)
{
	for (int i = 0; i < dest_len; i++)
	{
		int src_index = (i * rate) >> 10;
		if (src_index < src_len)
		{
			if (gain == 1024)
			{
				dest[i] = src[src_index];
			}
			else
			{
				StereoSampleBlock ssb = src[src_index];
				dest[i].l = applyGainWithLimit(ssb.l, gain);
				dest[i].r = applyGainWithLimit(ssb.r, gain);
			}
		}
		else
		{
			dest[i].l = 0;
			dest[i].r = 0;
		}
	}
}

void resampleWithGain(void * dest, int dest_len, void * src, int src_len, int channels, int rate, int gain)
{
	if (rate == 1024)
	{
		copyWithGain((short*) dest, dest_len, (short*) src, src_len, channels, gain);
	}
	else
	{
		if (channels == 1)
		{
			resampleWithGainMono((short*) dest, dest_len, (short*) src, src_len, rate, gain);
		}
		else if (channels == 2)
		{
			resampleWithGainStereo((StereoSampleBlock*) dest, dest_len, (StereoSampleBlock*) src, src_len, rate, gain);
		}
	}
}

void measureAudioLevelsMono(AudioLevel * level, short * data, int samples)
{
	long long sum  = 0;
	short     peak = 0;

	for (int i = 0; i < samples; i++)
	{
		short s = data[i];

		sum += s * s;

		if (qAbs(s) > peak)
			peak = qAbs(s);
	}

	level->left_rms   = qSqrt(sum / samples);
	level->left_peak  = peak;
	level->right_rms  = 0;
	level->right_peak = 0;
}

void measureAudioLevelsStereo(AudioLevel * level, StereoSampleBlock * data, int samples)
{
	long long sumL = 0;
	long long sumR = 0;
	short peakL = 0;
	short peakR = 0;

	for (int i = 0; i < samples; i++)
	{
		short l = data[i].l;
		short r = data[i].r;

		sumL += l * l;
		sumR += r * r;

		if (qAbs(l) > peakL)
			peakL = qAbs(l);
		if (qAbs(r) > peakR)
			peakR = qAbs(r);
	}

	level->left_rms   = qSqrt(sumL / samples);
	level->left_peak  = peakL;
	level->right_rms  = qSqrt(sumR / samples);
	level->right_peak = peakR;
}

void measureAudioLevels(AudioLevel * level, void * data, int channels, int samples)
{
	if (channels == 1)
	{
		measureAudioLevelsMono(level, (short*) data, samples);
	}
	else if (channels == 2)
	{
		measureAudioLevelsStereo(level, (StereoSampleBlock*) data, samples);
	}
}
