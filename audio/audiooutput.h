#ifndef AUDIOOUTPUT_H
#define AUDIOOUTPUT_H

struct ao_device;

class AudioOutput
{
public:
	AudioOutput();
	~AudioOutput();

	bool open(int channels, int samplerate);
	bool write(short * data, int samples_per_channel);
	void close();

private:
	ao_device * m_device;
	int         m_channels;
};

#endif // AUDIOOUTPUT_H
