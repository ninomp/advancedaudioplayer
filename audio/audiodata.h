#ifndef AUDIODATA_H
#define AUDIODATA_H

class AudioData
{
public:
	static AudioData * create(int channels, int samplerate, int sample_blocks);

	AudioData();
	~AudioData();

	bool createData(int channels, int samplerate, int sample_blocks);
	void deleteData();

	int channels() const
	{  return m_nChannels;  }

	int samplerate() const
	{  return m_nSamplerate;  }

	int samples() const
	{  return m_nSamples;  }

	short * data() const
	{  return m_pData;  }

private:
	int     m_nChannels;
	int     m_nSamplerate;
	int     m_nSamples;
	short * m_pData;
};

struct StereoSampleBlock
{
	short l;
	short r;
};

struct AudioLevel
{
	short left_rms;
	short left_peak;
	short right_rms;
	short right_peak;
};

void resampleWithGain(void * dest, int dest_len, void * src, int src_len, int channels, int rate /* / 1024 */, int gain /* / 1024*/);

void measureAudioLevels      (AudioLevel* /* out */, void              * data, int channels, int samples);
void measureAudioLevelsMono  (AudioLevel* /* out */, short             * data, int samples);
void measureAudioLevelsStereo(AudioLevel* /* out */, StereoSampleBlock * data, int samples);

#endif // AUDIODATA_H
