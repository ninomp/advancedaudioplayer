#include <ao/ao.h>

#include "audiooutput.h"

AudioOutput::AudioOutput()
{
	m_device   = 0;
	m_channels = 0;

	// Initialize audio output library
	ao_initialize();
}

AudioOutput::~AudioOutput()
{
	if (m_device)  ao_close(m_device);

	// Terminate audio output library
	ao_shutdown();
}

bool AudioOutput::open(int channels, int samplerate)
{
	ao_sample_format fmt;
	fmt.bits        = 16;
	fmt.rate        = samplerate;
	fmt.channels    = channels;
	fmt.byte_format = AO_FMT_NATIVE;
	fmt.matrix      = NULL;

	m_device   = ao_open_live(ao_default_driver_id(), &fmt, NULL);
	m_channels = channels;

	return (m_device != NULL) ? true : false;
}

bool AudioOutput::write(short *data, int samples_per_channel)
{
	return (bool) ao_play(m_device, (char*) data, sizeof(short) * m_channels * samples_per_channel);
}

void AudioOutput::close()
{
	if (m_device)  ao_close(m_device);
	m_device = 0;
}
