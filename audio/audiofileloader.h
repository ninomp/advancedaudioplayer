#ifndef AUDIOFILELOADER_H
#define AUDIOFILELOADER_H

#include "audiodata.h"

class AudioFileLoader
{
public:
	virtual AudioData * load(const char*) = 0;
};

#endif // AUDIOFILELOADER_H
